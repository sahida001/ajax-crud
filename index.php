<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Students All Data</title>
	<!-- ALL CSS FILES  -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body>
	
	<div class="menu  ">
		<div class="container mt-5">
			<div class="col-lg-12">
				<a id="all_student" class="btn btn-primary btn-sm" href="">All Students</a>
				<a id="add_student" class="btn btn-primary btn-sm" href="">Add new student</a>
				<a id="profile" class="btn btn-primary btn-sm" href="">Profile</a>
			</div>
		</div>
	</div>

	<div class="app">
	<div class="wrap-table shadow">
		<div class="card">
			<div class="card-body">
				<h2>All Data</h2>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Cell</th>
							<th>Photo</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Asraful Haque</td>
							<td>haq@gmail.com</td>
							<td>01717700811</td>
							<td><img src="assets/media/img/pp_photo/istockphoto-615279718-612x612.jpg" alt=""></td>
							<td>
								<a class="btn btn-sm btn-info" href="#">View</a>
								<a class="btn btn-sm btn-warning" href="#">Edit</a>
								<a class="btn btn-sm btn-danger" href="#">Delete</a>
							</td>
						</tr>
						<tr>
							<td>1</td>
							<td>Asraful Haque</td>
							<td>haq@gmail.com</td>
							<td>01717700811</td>
							<td><img src="assets/media/img/pp_photo/istockphoto-615279718-612x612.jpg" alt=""></td>
							<td>
								<a class="btn btn-sm btn-info" href="#">View</a>
								<a class="btn btn-sm btn-warning" href="#">Edit</a>
								<a class="btn btn-sm btn-danger" href="#">Delete</a>
							</td>
						</tr>
						<tr>
							<td>1</td>
							<td>Asraful Haque</td>
							<td>haq@gmail.com</td>
							<td>01717700811</td>
							<td><img src="assets/media/img/pp_photo/istockphoto-615279718-612x612.jpg" alt=""></td>
							<td>
								<a class="btn btn-sm btn-info" href="#">View</a>
								<a class="btn btn-sm btn-warning" href="#">Edit</a>
								<a class="btn btn-sm btn-danger" href="#">Delete</a>
							</td>
						</tr>
						<tr>
							<td>1</td>
							<td>Asraful Haque</td>
							<td>haq@gmail.com</td>
							<td>01717700811</td>
							<td><img src="assets/media/img/pp_photo/istockphoto-615279718-612x612.jpg" alt=""></td>
							<td>
								<a class="btn btn-sm btn-info" href="#">View</a>
								<a class="btn btn-sm btn-warning" href="#">Edit</a>
								<a class="btn btn-sm btn-danger" href="#">Delete</a>
							</td>
						</tr>
						<tr>
							<td>1</td>
							<td>Asraful Haque</td>
							<td>haq@gmail.com</td>
							<td>01717700811</td>
							<td><img src="assets/media/img/pp_photo/istockphoto-615279718-612x612.jpg" alt=""></td>
							<td>
								<a class="btn btn-sm btn-info" href="#">View</a>
								<a class="btn btn-sm btn-warning" href="#">Edit</a>
								<a class="btn btn-sm btn-danger" href="#">Delete</a>
							</td>
						</tr>
						

					</tbody>
				</table>
			</div>
		</div>
	</div>
	</div>
	




	<!-- JS FILES  -->
	<script src="assets/js/jquery-3.4.1.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/custom.js"></script>

	<script>
		$('#add_student').click(function(){ //jokhon kew add new student e click korbe tokhon ajax request kothao na kothao jabe. orthat ekta page e jabe.and oi page e ja pabe shob dhore niye ashbe.
			
			// ajax ta k ekhane call korlam. jokhon add student e click korbo tokhon ekta ajax request send hobe
			$.ajax({
            url:'create.php', //amra kon page e jabo sheta url e bole dite hobe.
            success: function(data){ //amra j url e page er nam ta bole dichi, oi page er data gula amra dhorte chai, data gula dhorbo success er moddhe  function er moddhe j data parameter ache, shei data parameter er moddhe information gula store hobe.	
			$('.app').html(data);
			}
			});
			return false; //ekhane return false mane add student and create.php er page gula load hobe na.
		});

		//profile
		$('#profile').click(function(){ 

			$.ajax({
            url:'profile.php', 
            success: function(data){ 	
			$('.app').html(data); // html function er moddhe data tmr moddhe ja ache shob app er moddhe eshe chere dao
			}
			});
			return false; 
		});

			//All student
			$('#all_student').click(function(){ 
			$.ajax({
			url:'all.php', 
			success: function(data){ 	
			$('.app').html(data); // html function er moddhe data tmr moddhe ja ache shob app er moddhe eshe chere dao
			}
			});
			return false; 
			});
	</script>
</body>
</html>